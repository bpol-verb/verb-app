/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import { clientsClaim } from 'workbox-core';
import { precacheAndRoute } from 'workbox-precaching';

// the fontCacheName is also used in useFonts.ts and must be changed there too
// importing it from useFonts.ts is not possible
export const fontCacheName = 'verb-fonts-v3-' + location.origin;

function isFont(url) {
  return (
    /.*\.OTF/.test(url) ||
    /.*\.TTF/.test(url)
  );
}

self.addEventListener('fetch', (event) => {
  if (isFont(event.request.url)) {
    event.respondWith(
      caches
        .match(event.request)
        .then((response) => {
          if (response) {
            return response;
          }

          return fetch(event.request)
            .then((response) => {
              if (response.status === 200 && isFont(event.request.url)) {
                return caches.open(fontCacheName).then((cache) => {
                  void cache.put(event.request.url, response.clone());

                  return response;
                });
              }

              return response;
            })
            .catch(() => response);
        })
        .catch((error) => {
          // TODO 6 - Respond with custom offline page
        })
    );
  }
});

self.addEventListener('activate', (event) => {
  const cacheAllowlist = [fontCacheName];

  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames
          .filter(
            (cacheName) =>
              !cacheName.startsWith('workbox-precache') &&
              cacheName.endsWith(location.origin)
          )
          .map((cacheName) => {
            if (cacheAllowlist.indexOf(cacheName) === -1) {
              return caches.delete(cacheName);
            }
          })
      );
    })
  );
});

// eslint-disable-next-line @typescript-eslint/no-misused-promises
self.addEventListener('activate', async () => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const tabs = await self.clients.matchAll({ type: 'window' });

  tabs.forEach((tab) => {
    tab.navigate(tab.url);
  });
});

self.addEventListener('install', () => {
  self.skipWaiting();
});

clientsClaim();
// cleanupOutdatedCaches();

// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
precacheAndRoute(self.__WB_MANIFEST);
