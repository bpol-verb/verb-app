import { date } from 'quasar';
import * as ZXing from '@zxing/browser';

describe('Result Page', () => {
  // fixed to ensure content of QR code is always the same for tests
  const fixedTimestamp: number = Date.UTC(2022, 6, 15, 12, 0, 0, 0);

  const initializeAnswers = (answers: string, timestamp?: number) => {
    cy.window().then((window) => {
      window.localStorage.setItem('state', 'done');
      window.localStorage.setItem(
        'selections',
        '[1,' +
          ((timestamp ?? fixedTimestamp) / 1000).toString() +
          ',' +
          answers +
          ']'
      );
    });
  };

  it('calculates valid date', () => {
    const timestamp: number = Date.UTC(2032, 6, 15, 12, 0, 0, 0);

    initializeAnswers('0,0,0,[0,1],0,0,0,[0,1,2]', timestamp);
    cy.visit('/');

    const validDate = date.addToDate(timestamp, { days: 2 });
    validDate.setHours(23, 59, 59, 999);

    selectLanguage(1);

    cy.dataCy('valid-date').should(
      'contain',
      date.formatDate(validDate, 'DD.MM.YYYY') + ', 23:59'
    );
  });

  it('QR code contains encoded string', () => {
    initializeAnswers('0,0,0,[0,1],0,0,0,[0,1,2]');
    cy.visit('/');

    cy.dataCy('qrcode').then(($el) => {
      const canvas: HTMLCanvasElement = $el[0] as HTMLCanvasElement;

      const decoder = new ZXing.BrowserQRCodeReader();
      const result = decoder.decodeFromCanvas(canvas);

      expect(result.getText()).to.eq('BP:4KH4F3+KQCCO000PJGV50000BPGX50');
    });
  });

  it('ensures QR code uses H error correction level', () => {
    initializeAnswers('0,0,0,[0,1],0,0,0,[0,1,2]');
    cy.visit('/');

    cy.dataCy('qrcode').then(($el) => {
      const canvas: HTMLCanvasElement = $el[0] as HTMLCanvasElement;

      const decoder = new ZXing.BrowserQRCodeReader();
      const result = decoder.decodeFromCanvas(canvas);

      expect(result.getResultMetadata().get(3)).to.eq('H');
    });
  });

  it('ensures QR code uses Q error correction level with all selections', () => {
    initializeAnswers(
      '0,0,0,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25],0,0,0,[0,1,2]'
    );
    cy.visit('/');

    cy.dataCy('qrcode').then(($el) => {
      const canvas: HTMLCanvasElement = $el[0] as HTMLCanvasElement;

      const decoder = new ZXing.BrowserQRCodeReader();
      const result = decoder.decodeFromCanvas(canvas);

      expect(result.getResultMetadata().get(3)).to.eq('Q');
    });
  });

  it('clears selections and returns to start after new survey', () => {
    initializeAnswers('0,0,0,[0,1],0,0,0,[0,1,2]');
    cy.visit('/');

    cy.dataCy('confirm-selections').first().click();
    cy.dataCy('submit-survey').first().click();
    cy.dataCy('restart').first().click();

    cy.testRoute('/');
    cy.window().then((window) => {
      expect(window.localStorage.getItem('state')).to.eq('initial');
      expect(window.localStorage.getItem('selections')).to.eq(null);
    });
  });

  it('clears selections and returns to start after home', () => {
    initializeAnswers('0,0,0,[0,1],0,0,0,[0,1,2]');
    cy.visit('/');

    cy.dataCy('home').first().click();

    cy.testRoute('/');
    cy.window().then((window) => {
      expect(window.localStorage.getItem('state')).to.eq('initial');
      expect(window.localStorage.getItem('selections')).to.eq(null);
    });
  });
});

export function selectLanguage(index: number) {
  cy.dataCy('language-selection').first().click();
  cy.dataCy('language-selection').withinMenu(() => {
    cy.dataCy('language-selection-option').then((items) => {
      if (index < items.length) {
        cy.wait(50).then(() => items[index].click());
      }
    });
  });
}
