describe('Survey', () => {
  beforeEach(() => {
    cy.window().then((window) => {
      window.localStorage.setItem('state', 'survey');
    });

    cy.visit('/');
  });

  it('loads survey page', () => {
    cy.testRoute('survey');
  });

  it('selects and stores answers', () => {
    selectRadioOption(0, 0);
    selectRadioOption(1, 0);
    selectRadioOption(2, 0);
    selectCheckboxOption(3, 0);
    selectCheckboxOption(3, 1);
    selectRadioOption(4, 0);
    selectRadioOption(5, 0);
    selectRadioOption(6, 0);
    selectCheckboxOption(7, 0);
    selectCheckboxOption(7, 1);
    selectCheckboxOption(7, 2);

    const now = new Date();
    const timestamp = now.getTime().toString();
    const partialTimestamp = timestamp.substring(0, timestamp.length - 6);

    cy.window().then((window) => {
      expect(window.localStorage.getItem('selections')).to.match(
        new RegExp(
          '\\[[0-9]+,' +
            partialTimestamp +
            '[0-9]{3},0,0,0,\\[0,1\\],0,0,0,\\[0,1,2\\]\\]'
        )
      );
    });
  });

  it('shows results after all questions answered and submitted', () => {
    selectRadioOption(0, 0);
    selectRadioOption(1, 0);
    selectRadioOption(2, 0);
    selectCheckboxOption(3, 0);
    selectCheckboxOption(3, 1);
    selectRadioOption(4, 0);
    selectRadioOption(5, 0);
    selectRadioOption(6, 0);
    selectCheckboxOption(7, 0);
    selectCheckboxOption(7, 1);
    selectCheckboxOption(7, 2);

    cy.dataCy('submit-survey').first().click();

    cy.testRoute('results');

    const now = new Date();
    const timestamp = now.getTime().toString();
    const partialTimestamp = timestamp.substring(0, timestamp.length - 6);

    cy.window().then((window) => {
      expect(window.localStorage.getItem('selections')).to.match(
        new RegExp(
          '\\[[0-9]+,' +
            partialTimestamp +
            '[0-9]{3},0,0,0,\\[0,1\\],0,0,0,\\[0,1,2\\]\\]'
        )
      );

      expect(window.localStorage.getItem('state')).to.eq('done');
    });
  });

  it('does not show results if not all questions are answered', () => {
    cy.dataCy('submit-survey').first().click();

    cy.testRoute('survey');

    selectRadioOption(0, 0);
    selectRadioOption(1, 0);
    selectRadioOption(2, 0);

    cy.dataCy('submit-survey').first().click();

    cy.testRoute('survey');

    selectCheckboxOption(3, 0);
    selectCheckboxOption(3, 1);
    selectRadioOption(4, 0);
    selectRadioOption(5, 0);
    selectRadioOption(6, 0);

    cy.dataCy('submit-survey').first().click();

    cy.testRoute('survey');
  });

  it('opens info dialog and closes it', () => {
    cy.dataCy('header-info').first().click();

    cy.contains('Information');

    cy.dataCy('close-info').first().click();

    cy.should('not.contain', 'Information');
  });
});

function selectRadioOption(questionIndex: number, optionIndex: number) {
  // wait is necessary in electron browser. Otherwise selections are not correctly saved
  cy.wait(50).then(() => {
    cy.dataCy('options' + questionIndex.toString()).within(() => {
      cy.get('.q-radio').then((items) => {
        if (optionIndex < items.length) {
          items[optionIndex].click();
        }
      });
    });
  });
}

function selectCheckboxOption(questionIndex: number, optionIndex: number) {
  // wait is necessary in electron browser. Otherwise selections are not correctly saved
  cy.wait(50).then(() => {
    cy.dataCy('options' + questionIndex.toString()).within(() => {
      cy.get('.q-checkbox').then((items) => {
        if (optionIndex < items.length) {
          items[optionIndex].click();
        }
      });
    });
  });
}
