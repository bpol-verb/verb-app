// Use `cy.dataCy` custom command for more robust tests
// See https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements

// ** This file is an example of how to write Cypress tests, you can safely delete it **

// This test will pass when run against a clean Quasar project

describe('Landing Page', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.dataCy('close-info').first().click();
  });

  it('allows selection of all supported languages', () => {
    cy.dataCy('language-selection').first().click();

    cy.dataCy('language-selection-option').then((items) => {
      expect(items[0]).to.contain.text('العربية');
      expect(items[1]).to.contain.text('Deutsch');
      expect(items[2]).to.contain.text('English');
      expect(items[3]).to.contain.text('Español');
      expect(items[4]).to.contain.text('Français');
      expect(items[5]).to.contain.text('हिन्दी');
      expect(items[6]).to.contain.text('日本語 (にほんご)');
      expect(items[7]).to.contain.text('ქართული');
      expect(items[8]).to.contain.text('Português');
      expect(items[9]).to.contain.text('русский');
      expect(items[10]).to.contain.text('shqip');
      expect(items[11]).to.contain.text('Türkçe');
      expect(items[12]).to.contain.text('標準漢語');
    });
  });

  it('starts precheck', () => {
    cy.dataCy('start-questionaire').should('be.disabled');

    cy.dataCy('checkbox_privacy_confirmation').first().click();

    cy.dataCy('start-questionaire').first().click();

    cy.testRoute('precheck');
    cy.window().then((window) => {
      expect(window.localStorage.getItem('state')).to.eq('precheck');
    });
  });
});
