describe('Language Selection', { browser: 'electron' }, () => {
  let notoSansCount = 0;
  let notoSansCountSC = 0;
  let notoSansCountJP = 0;
  let notoSansCountArabic = 0;
  let notoSansCountGeorgian = 0;

  cy.on('fail', (e) => {
    if (
      e.name === 'CypressError' &&
      e.message.includes('timed out') &&
      e.message.includes('remote:debugger:protocol') &&
      e.message.includes('Network.emulateNetworkConditions') &&
      (e.message.includes('offline: true') ||
        e.message.includes('offline: false'))
    ) {
      // do not fail the test
      return false;
    }
  });

  const goOffline = () => {
    cy.log('**go offline**')
      .then(() => {
        return Cypress.automation('remote:debugger:protocol', {
          command: 'Network.enable',
        });
      })
      .then(() => {
        return Cypress.automation('remote:debugger:protocol', {
          command: 'Network.emulateNetworkConditions',
          params: {
            offline: true,
            latency: -1,
            downloadThroughput: -1,
            uploadThroughput: -1,
          },
        });
      });
  };

  const goOnline = () => {
    // disable offline mode, otherwise we will break our tests :)
    cy.log('**go online**')
      .then(() => {
        // https://chromedevtools.github.io/devtools-protocol/1-3/Network/#method-emulateNetworkConditions
        return Cypress.automation('remote:debugger:protocol', {
          command: 'Network.emulateNetworkConditions',
          params: {
            offline: false,
            latency: -1,
            downloadThroughput: -1,
            uploadThroughput: -1,
          },
        });
      })
      .then(() => {
        return Cypress.automation('remote:debugger:protocol', {
          command: 'Network.disable',
        });
      });
  };

  const assertOnline = () => {
    return cy.wrap(window).its('navigator.onLine').should('be.true');
  };

  const assertOffline = () => {
    return cy.wrap(window).its('navigator.onLine').should('be.false');
  };

  beforeEach(() => {
    goOnline();
    assertOnline();

    cy.window().then(async (window) => {
      await window.caches.keys().then(async (keys) => {
        for (let i = 0; i < keys.length; i++) {
          if (keys[i].includes('verb-fonts')) {
            await window.caches.delete(keys[i]);
          }
        }
      });
    });
  });

  afterEach(() => {
    goOnline();
    assertOnline();
  });

  it('loads NotoSans for German, English, French, Spanish, etc.', { retries: 3, waitForAnimations: true }, () => {
    cy.intercept('**/NotoSans-Regular*.TTF', (req) => {
      notoSansCount += 1;
      req.continue();
    }).as('notoSansRegular');

    cy.intercept('**/NotoSansSC*.OTF', (req) => {
      notoSansCountSC += 1;
      req.continue();
    }).as('notoSansSC');

    cy.intercept('**/NotoSansJP*.OTF', (req) => {
      notoSansCountJP += 1;
      req.continue();
    }).as('notoSansJP');

    cy.intercept('**/NotoSansArabic*.TTF', (req) => {
      console.log(req);
      notoSansCountArabic += 1;
      req.continue();
    }).as('notoSansArabic');

    cy.intercept('**/NotoSansGeorgian*.TTF', (req) => {
      notoSansCountGeorgian += 1;
      req.continue();
    }).as('notoSansGeorgian');

    cy.visit('/');
    cy.dataCy('close-info').first().click();

    // NotoSansRegular is loaded by default so German is always locally available
    cy.wait('@notoSansRegular');

    for (let i = 0; i < 13; i++) {
      selectLanguage(i);
    }

    cy.dataCy('language-selection').then(() =>
      expect(notoSansCount).to.equal(1)
    );

    cy.wait('@notoSansSC').then(() => {
      expect(notoSansCountSC).to.equal(1);
    });

    cy.wait('@notoSansArabic').then(() => {
      expect(notoSansCountArabic).to.equal(1);
    });

    cy.wait('@notoSansGeorgian').then(() => {
      expect(notoSansCountGeorgian).to.equal(1);
    });

    cy.wait('@notoSansJP').then(() => {
      expect(notoSansCountJP).to.equal(1);
    });

    for (let i = 0; i < 13; i++) {
      selectLanguage(i);
    }

    cy.wait(1500).then(() => {
      expect(notoSansCount).to.equal(1);
      expect(notoSansCountSC).to.equal(1);
      expect(notoSansCountArabic).to.equal(1);
      expect(notoSansCountGeorgian).to.equal(1);
      expect(notoSansCountJP).to.equal(1);
    });
  });

  it('Displays offline languages for Noto-SansRegular', { retries: 3, waitForAnimations: true }, () => {
    cy.intercept('**/NotoSans-Regular*.TTF').as('notoSansRegular');

    cy.visit('/');
    cy.dataCy('close-info').first().click();

    cy.wait('@notoSansRegular').then(() => {
      goOffline();
      assertOffline();

      cy.dataCy('language-selection').first().click();
      cy.wait(500);
      selectLanguage(6, false);
      cy.wait(500);
      cy.dataCy('language-selection').first().click();
      cy.wait(500);

      cy.dataCy('language-selection-option')
        .should('contain', 'Deutsch')
        .and('contain', 'English')
        .and('contain', 'Español')
        .and('contain', 'Français')
        .and('contain', 'हिन्दी')
        .and('contain', 'Português')
        .and('contain', 'русский')
        .and('contain', 'shqip')
        .and('contain', 'Türkçe')
        .and('not.contain', 'العربية')
        .and('not.contain', 'ქართული')
        .and('not.contain', '日本語 (にほんご)');
    });
  });

  it('Displays offline languages for Noto-SansJP', { retries: 3, waitForAnimations: true }, () => {
    cy.intercept('**/NotoSansJP*.OTF').as('notoSansJP');

    cy.visit('/');
    cy.dataCy('close-info').first().click();

    selectLanguage(6);

    cy.wait('@notoSansJP').then(() => {
      goOffline();
      assertOffline();

      cy.dataCy('language-selection').first().click();
      cy.wait(500);
      selectLanguage(6, false);
      cy.wait(500);
      cy.dataCy('language-selection').first().click();
      cy.wait(500);

      cy.dataCy('language-selection-option')
        .should('contain', '日本語 (にほんご)')
        .and('not.contain', 'العربية')
        .and('not.contain', 'ქართული');
    });
  });

  it('Displays offline languages for Noto-SansArabic', { retries: 3, waitForAnimations: true }, () => {
    cy.intercept('**/NotoSansArabic*.TTF').as('notoSansArabic');

    cy.visit('/');
    cy.dataCy('close-info').first().click();

    selectLanguage(0);

    cy.wait('@notoSansArabic').then(() => {
      goOffline();
      assertOffline();

      cy.dataCy('language-selection').first().click();
      cy.wait(500);
      selectLanguage(6, false);
      cy.wait(500);
      cy.dataCy('language-selection').first().click();
      cy.wait(500);

      cy.dataCy('language-selection-option')
        .should('contain', 'العربية')
        .and('not.contain', '日本語 (にほんご)')
        .and('not.contain', 'ქართული')
        .and('not.contain', '標準漢語');
    });
  });

  it('Displays offline languages for Noto-SansGeorgian', { retries: 3, waitForAnimations: true }, () => {
    cy.intercept('**/NotoSansGeorgian*.TTF').as('notoSansGeorgian');

    cy.visit('/');
    cy.dataCy('close-info').first().click();

    selectLanguage(7);

    cy.wait('@notoSansGeorgian').then(() => {
      goOffline();
      assertOffline();

      cy.dataCy('language-selection').first().click();
      cy.wait(500);
      selectLanguage(6, false);
      cy.wait(500);
      cy.dataCy('language-selection').first().click();
      cy.wait(500);

      cy.dataCy('language-selection-option')
        .should('contain', 'ქართული')
        .and('not.contain', '日本語 (にほんご)')
        .and('not.contain', 'العربية')
        .and('not.contain', '標準漢語');
    });
  });

  it('Displays offline languages for Noto-SansSC', { retries: 3, waitForAnimations: true }, () => {
    cy.intercept('**/NotoSansSC*.OTF').as('notoSansSC');

    cy.visit('/');
    cy.dataCy('close-info').first().click();

    selectLanguage(12);

    cy.wait('@notoSansSC').then(() => {
      goOffline();
      assertOffline();

      cy.wait(1500);
      cy.dataCy('language-selection').first().click();
      cy.wait(500);

      cy.dataCy('language-selection-option')
        .should('contain', '標準漢語')
        .and('not.contain', '日本語 (にほんご)')
        .and('not.contain', 'ქართული')
        .and('not.contain', 'العربية');
    });
  });
});

export function selectLanguage(index: number, closeInfo = true) {
  cy.dataCy('language-selection').first().click();
  cy.dataCy('language-selection').withinMenu(() => {
    cy.dataCy('language-selection-option').then((items) => {
      if (index < items.length) {
        cy.wait(50).then(() => items[index].click());
      }
    });
  });

  if (closeInfo) {
    cy.dataCy('close-info').first().click();
  }
}
