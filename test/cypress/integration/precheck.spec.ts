describe('Precheck', () => {
  beforeEach(() => {
    cy.window().then((window) => {
      window.localStorage.setItem('state', 'precheck');
    });

    cy.visit('/');
  });

  it('loads precheck page', () => {
    cy.testRoute('precheck');
  });

  it('goes to end of precheck if question 1 is answered with yes', () => {
    selectOption(0, 0);

    assertNotRelevant('euCitizens');
  });

  it('goes to end of precheck if question 2 is answered with no', () => {
    selectOption(0, 1);
    selectOption(1, 1);

    assertNotRelevant('deuEntry');
  });

  it('goes to end of precheck if question 3 is answered with yes', () => {
    selectOption(0, 1);
    selectOption(1, 0);
    selectOption(2, 0);

    assertNotRelevant('euCard');
  });

  it('goes to end of precheck if question 4 is answered with yes', () => {
    selectOption(0, 1);
    selectOption(1, 0);
    selectOption(2, 1);
    selectOption(3, 0);

    assertNotRelevant('euVisa');
  });

  it('goes to survey if all questions are answered correctly', () => {
    selectOption(0, 1);
    selectOption(1, 0);
    selectOption(2, 1);
    selectOption(3, 1);

    cy.testRoute('survey');
    cy.window().then((window) => {
      expect(window.localStorage.getItem('state')).to.eq('survey');
    });
  });

  it('opens info dialog and closes it', () => {
    cy.dataCy('header-info').first().click();

    cy.contains('Information');

    cy.dataCy('close-info').first().click();

    cy.should('not.contain', 'Information');
  });
});

function selectOption(questionIndex: number, optionIndex: number) {
  cy.dataCy('options' + questionIndex.toString())
    .get('.q-radio')
    .then((items) => {
      if (optionIndex < items.length) {
        items[optionIndex].click();
      }
    });

  cy.dataCy('submit-precheck').first().click();
}

function assertNotRelevant(reason: string) {
  cy.testRoute('not-relevant/' + reason);
  cy.window().then((window) => {
    expect(window.localStorage.getItem('state')).to.eq('initial');
  });
}
