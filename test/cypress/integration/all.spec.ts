describe('all pages', () => {
  it('allows opening help page', () => {
    cy.visit('/');
    cy.dataCy('close-info').first().click();

    cy.dataCy('help-selection').first().click();
    cy.testRoute('/help');
    cy.dataCy('back-button').first().click();
    cy.testRoute('/');
  });

  it('asserts home button works', () => {
    cy.visit('/');
    cy.dataCy('close-info').first().click();

    cy.dataCy('home-button').first().click();
    cy.testRoute('/');
  });

  it('asserts imprint link', () => {
    cy.visit('/');
    cy.dataCy('close-info').first().click();

    cy.dataCy('imprint-link').first().click();
    cy.testRoute('/imprint');
  });

  it('allows selection of all supported languages', () => {
    cy.visit('/');
    cy.dataCy('close-info').first().click();

    cy.dataCy('language-selection').first().click();

    cy.dataCy('language-selection-option').then((items) => {
      expect(items[0]).to.contain.text('العربية');
      expect(items[1]).to.contain.text('Deutsch');
      expect(items[2]).to.contain.text('English');
      expect(items[3]).to.contain.text('Español');
      expect(items[4]).to.contain.text('Français');
      expect(items[5]).to.contain.text('हिन्दी');
      expect(items[6]).to.contain.text('日本語 (にほんご)');
      expect(items[7]).to.contain.text('ქართული');
      expect(items[8]).to.contain.text('Português');
      expect(items[9]).to.contain.text('русский');
      expect(items[10]).to.contain.text('shqip');
      expect(items[11]).to.contain.text('Türkçe');
      expect(items[12]).to.contain.text('標準漢語');
    });
  });
});
