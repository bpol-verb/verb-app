import { useVerbController } from 'src/composables/useVerbController';
import { RouteRecordRaw } from 'vue-router';

export const HOME_ROUTE = 'Home';
export const PRECHECK_ROUTE = 'Precheck';
export const SURVEY_ROUTE = 'Survey';
export const EDIT_ROUTE = 'EditSurvey';
export const RESULT_ROUTE = 'Result';
export const NOT_RELEVANT_ROUTE = 'NotRelevant';
export const IMPRINT_ROUTE = 'Imprint';
export const HELP_ROUTE = 'Help';
export const PRIVACY_ROUTE = 'Privacy';
export const INVALID_ROUTE = 'Invalid';

const controller = useVerbController();

function checkState(match: () => boolean) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (to: any) => {
    if (match()) {
      return true;
    }

    if (controller.isDoneState()) {
      return { name: RESULT_ROUTE };
    } else if (controller.isSurveyState()) {
      return { name: SURVEY_ROUTE };
    } else if (controller.isEditState()) {
      return { name: EDIT_ROUTE };
    } else if (controller.isPrecheckState()) {
      return { name: PRECHECK_ROUTE };
    }

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (to.name === HOME_ROUTE) {
      return true;
    }

    controller.returnToHome();

    return { name: HOME_ROUTE };
  };
}

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: HOME_ROUTE,
        path: '',
        beforeEnter: checkState(
          () => controller.isInitialState() || controller.isUnknownState()
        ),
        component: () => import('pages/Index.vue'),
      },
    ],
  },

  {
    path: '/precheck',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: checkState(() => controller.isPrecheckState()),
    children: [
      {
        name: PRECHECK_ROUTE,
        path: '',
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/PrecheckPage.vue'),
      },
    ],
  },

  {
    path: '/survey',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: checkState(() => controller.isSurveyState()),
    children: [
      {
        name: SURVEY_ROUTE,
        path: '',
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/SurveyPage.vue'),
      },
    ],
  },

  {
    path: '/edit',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: checkState(() => controller.isEditState()),
    children: [
      {
        name: EDIT_ROUTE,
        path: '',
        component: () => import('pages/EditSurveyPage.vue'),
      },
    ],
  },

  {
    path: '/results',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: checkState(() => controller.isDoneState()),
    children: [
      {
        name: RESULT_ROUTE,
        path: '',
        component: () => import('pages/ResultPage.vue'),
      },
    ],
  },

  {
    path: '/not-relevant',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: NOT_RELEVANT_ROUTE,
        path: ':reason',
        props: true,
        component: () => import('pages/NotRelevantPage.vue'),
      },
    ],
  },

  {
    path: '/new-survey',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: INVALID_ROUTE,
        path: '',
        component: () => import('pages/InvalidSelectionsPage.vue'),
      },
    ],
  },

  {
    path: '/help',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: HELP_ROUTE,
        path: '',
        component: () => import('pages/HelpPage.vue'),
      },
    ],
  },

  {
    path: '/privacy',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: PRIVACY_ROUTE,
        path: '',
        component: () => import('pages/PrivacyPage.vue'),
      },
    ],
  },

  {
    path: '/imprint',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: IMPRINT_ROUTE,
        path: '',
        component: () => import('pages/ImprintPage.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    redirect: { name: HOME_ROUTE },
  },
];

export default routes;
