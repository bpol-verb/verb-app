/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
// the fs here is not node fs but the provided virtual one
import fs from 'fs';

function registerBinaryFiles(ctx) {
  ctx.keys().forEach((key) => {
    // extracts "./" from beginning of the key
    fs.writeFileSync(key.substring(2), ctx(key));
  });
}

function registerAFMFonts(ctx) {
  ctx.keys().forEach((key) => {
    const match = key.match(/([^/]*\.afm$)/);
    if (match) {
      // afm files must be stored on data path
      fs.writeFileSync(`data/${match[0]}`, ctx(key));
    }
  });
}

registerAFMFonts(require.context('pdfkit/js/data', false, /Helvetica\.afm$/));

// register all files found in assets folder (relative to src)
registerBinaryFiles(require.context('./static-assets', true));
