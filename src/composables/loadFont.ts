/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/restrict-plus-operands */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import fs from 'fs';
import { fetchFile } from 'src/composables/httpHelpers';

export function loadFont(path: string) {
  return import('src/lazy-assets/' + path).then((val) => {
    return registerBinaryFiles(val.default, path);
  });
}

function registerBinaryFiles(httpPath: string, path: string) {
  return fetchFile(httpPath).then((fontData: any) => {
    fs.writeFileSync(path, fontData);
  });
}
