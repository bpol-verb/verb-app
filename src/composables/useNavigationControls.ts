import { useI18n } from 'vue-i18n';
import { useVerbRouter } from 'src/composables/useVerbRouter';
import { useVerbController } from 'src/composables/useVerbController';
import { copyToClipboard, Notify } from 'quasar';

const shareLink = location.origin;

export function useNavigationControls() {
  const { t } = useI18n();
  const router = useVerbRouter();
  const controller = useVerbController();

  function toHome() {
    controller.returnToHome();
    router.toHome();
  }

  function toHelp() {
    router.toHelp();
  }

  function toImprint() {
    router.toImprint();
  }

  function toPrivacy() {
    router.toPrivacy();
  }

  function toPrecheck() {
    controller.startQuestionaire();
    router.toPrecheck();
  }

  function shareToClipboard() {
    void copyToClipboard(shareLink)
      .then(() => {
        Notify.create({
          message: t('share_clipboard', { url: shareLink }),
        });
      })
      .catch(() => {
        Notify.create({
          message: t('share_not_available'),
        });
      });
  }

  function share() {
    try {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access
      (navigator as any)
        .share({
          url: shareLink,
          title: t('share_title'),
          text: t('share_text'),
        })
        .catch(() => {
          shareToClipboard();

          // apparently this fixes share on iOS
          window.location.reload();
        });
    } catch {
      shareToClipboard();
    }
  }

  return {
    share,
    toHelp,
    toHome,
    toImprint,
    toPrivacy,
    toPrecheck,
  };
}
