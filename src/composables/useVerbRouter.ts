import {
  HOME_ROUTE,
  IMPRINT_ROUTE,
  HELP_ROUTE,
  PRIVACY_ROUTE,
  NOT_RELEVANT_ROUTE,
  EDIT_ROUTE,
  INVALID_ROUTE,
} from './../router/routes';
import { PRECHECK_ROUTE, RESULT_ROUTE, SURVEY_ROUTE } from '../router/routes';
import { useRouter } from 'vue-router';

export function useVerbRouter() {
  const router = useRouter();

  function toHome() {
    void router.replace({ name: HOME_ROUTE });
  }

  function toPrecheck() {
    void router.replace({ name: PRECHECK_ROUTE });
  }

  function toSurvey() {
    void router.replace({ name: SURVEY_ROUTE });
  }

  function toEditSurvey() {
    void router.replace({ name: EDIT_ROUTE });
  }

  function toResult() {
    void router.replace({ name: RESULT_ROUTE });
  }

  function toNotRelevant(reason: string) {
    void router.replace({ name: NOT_RELEVANT_ROUTE, params: { reason } });
  }

  function toInvalidSurvey() {
    void router.replace({ name: INVALID_ROUTE });
  }

  function toPrivacy() {
    void router.push({ name: PRIVACY_ROUTE });
  }

  function toHelp() {
    void router.push({ name: HELP_ROUTE });
  }

  function toImprint() {
    void router.push({ name: IMPRINT_ROUTE });
  }

  return {
    toHome,
    toHelp,
    toSurvey,
    toResult,
    toPrivacy,
    toImprint,
    toPrecheck,
    toEditSurvey,
    toNotRelevant,
    toInvalidSurvey,
  };
}
