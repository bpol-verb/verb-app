/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
function createFetchError(fileURL: string, error: any) {
  const result = new Error(`Fetching "${fileURL}" failed: ${error}`);
  result.name = 'FetchError';
  return result;
}

export function fetchFile(fileURL: string, { type = 'arraybuffer' } = {}) {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open('GET', fileURL, true);
    request.responseType = type as XMLHttpRequestResponseType;

    request.onload = function () {
      if (request.status === 200) {
        resolve(request.response);
      } else {
        reject(createFetchError(fileURL, request.statusText));
      }
    };

    request.onerror = (error) => reject(createFetchError(fileURL, error));

    request.send();
  });
}
