/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function useQuestionTranslator(t: any) {
  function precheckQuestionText(questionIndex: number): string {
    return t('precheck' + (questionIndex + 1).toString());
  }

  function precheckOptionText(
    questionIndex: number,
    optionIndex: number
  ): string {
    return t(
      'precheck' +
        (questionIndex + 1).toString() +
        '_option' +
        (optionIndex + 1).toString()
    );
  }

  function surveyQuestionText(questionIndex: number, locale?: string): string {
    return t('question' + (questionIndex + 1).toString(), 1, {
      locale: locale,
    });
  }

  function surveyOptionText(
    questionIndex: number,
    optionIndex: number,
    locale?: string
  ): string {
    return t(
      'question' +
        (questionIndex + 1).toString() +
        '_option' +
        (optionIndex + 1).toString(),
      1,
      { locale: locale }
    );
  }

  return {
    precheckQuestionText,
    precheckOptionText,
    surveyQuestionText,
    surveyOptionText,
  };
}
