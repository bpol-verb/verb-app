import {
  SurveyOptionSelection,
  VersionedSurveySelections,
} from 'src/model/Survey';
import { Questionaire } from 'src/model/Survey';
import surveySchema from 'src/assets/survey_schema.json';
import Ajv from 'ajv';

const state_key = 'state';
const selections_key = 'selections';

const privacy_confirmation = 'privacy_confirmation';

const initial_state = 'initial';
const precheck_state = 'precheck';
const survey_state = 'survey';
const done_state = 'done';
const edit_state = 'edit';

const ajv = new Ajv();
const validateSurveySchema = ajv.compile(surveySchema);

export function useVerbController() {
  // eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-unsafe-assignment
  const questionaire: Questionaire = require('assets/survey.json');

  function getVerbVersion(): string {
    const version = process.env.version ?? 'unknown version';

    if (!version.startsWith(questionaire.version.toString())) {
      console.error('Version mismatch detected');
    }

    return version;
  }

  function setState(state: string) {
    localStorage.setItem(state_key, state);
  }

  function clearSelections() {
    localStorage.removeItem(selections_key);
  }

  function returnToHome() {
    setState(initial_state);
    clearSelections();
  }

  function startQuestionaire() {
    setState(precheck_state);
    clearSelections();
  }

  function finishPrecheck() {
    setState(survey_state);
    clearSelections();
  }

  function finishSurvey() {
    setState(done_state);
  }

  function precheckNotRelevant() {
    setState(initial_state);
    clearSelections();
  }

  function editSurveySelections() {
    setState(edit_state);
  }

  function versionedSurveySelectionsInvalid(
    versionedSurveySelections: VersionedSurveySelections
  ): boolean {
    if (!validateSurveySchema(versionedSurveySelections)) {
      return true;
    }

    const version = getVersion(versionedSurveySelections);

    return version !== questionaire.version;
  }

  function getValidatedVersionedSurveySelections():
    | VersionedSurveySelections
    | undefined {
    const versionedSelectionsValue: string | null =
      localStorage.getItem(selections_key);

    if (!versionedSelectionsValue) {
      return [];
    }

    let versionedSurveySelections: VersionedSurveySelections;

    try {
      versionedSurveySelections = JSON.parse(
        versionedSelectionsValue
      ) as VersionedSurveySelections;
    } catch (error) {
      return undefined;
    }

    if (versionedSurveySelectionsInvalid(versionedSurveySelections)) {
      return undefined;
    }

    return versionedSurveySelections;
  }

  function getSurveySelections(): SurveyOptionSelection[] {
    const versionedSelections: VersionedSurveySelections =
      getValidatedVersionedSurveySelections() ?? [];

    return versionedSelections.slice(2) as SurveyOptionSelection[];
  }

  function getVersionedSurveySelections(): VersionedSurveySelections {
    return getValidatedVersionedSurveySelections() ?? [];
  }

  function hasValidSelections(): boolean {
    return getValidatedVersionedSurveySelections() !== undefined;
  }

  function storeVersionedSurveySelections(
    key: string,
    selections: SurveyOptionSelection[]
  ) {
    const filledOutTimestamp: number = getFilledOutTimestamp();

    const versionedSelections = [
      questionaire.version,
      filledOutTimestamp,
      ...selections,
    ];

    localStorage.setItem(key, JSON.stringify(versionedSelections));
  }

  function getFilledOutTimestamp(): number {
    const now = new Date();

    return Math.floor(now.getTime() / 1000);
  }

  function getValidUntilDate(selections: VersionedSurveySelections): Date {
    const offset48HoursInMilliseconds = 48 * 60 * 60 * 1000;

    const date = new Date(
      (selections[1] as number) * 1000 + offset48HoursInMilliseconds
    );
    date.setHours(23, 59, 59, 999);

    return date;
  }

  function getVersion(selections: VersionedSurveySelections): number {
    return selections[0] as number;
  }

  function saveSelections(selections: SurveyOptionSelection[]) {
    const sortedSelections = selections.map((selection) => {
      if (selection instanceof Array) {
        return selection.sort((a, b) => a - b);
      }

      return selection;
    });

    storeVersionedSurveySelections(selections_key, sortedSelections);
  }

  function isState(state: string | null): boolean {
    return localStorage.getItem(state_key) === state;
  }

  function isSurveyState(): boolean {
    return isState(survey_state);
  }

  function isDoneState(): boolean {
    return isState(done_state);
  }

  function isEditState(): boolean {
    return isState(edit_state);
  }

  function isPrecheckState(): boolean {
    return isState(precheck_state);
  }

  function isInitialState(): boolean {
    return isState(initial_state);
  }

  function isUnknownState(): boolean {
    return isState(null);
  }

  function setPrivacyConfirmation(value: boolean) {
    localStorage.setItem(privacy_confirmation, String(value));
  }

  function getPrivacyConfirmation(): boolean {
    return localStorage.getItem(privacy_confirmation) === 'true';
  }

  return {
    isDoneState,
    isEditState,
    finishSurvey,
    questionaire,
    returnToHome,
    isSurveyState,
    getVerbVersion,
    finishPrecheck,
    saveSelections,
    isInitialState,
    isUnknownState,
    isPrecheckState,
    getValidUntilDate,
    startQuestionaire,
    hasValidSelections,
    getSurveySelections,
    precheckNotRelevant,
    editSurveySelections,
    setPrivacyConfirmation,
    getPrivacyConfirmation,
    getVersionedSurveySelections,
  };
}
