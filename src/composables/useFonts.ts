/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { localeOptions } from 'src/assets/languages';
import { loadFont } from 'src/composables/loadFont';
import { ref } from 'vue';
import { LocaleOption } from './../assets/languages';

// the fontCacheName is also used in custom-service-worker.js and must be changed there too
// importing it from custom-service-worker.js is not possible
export const fontCacheName = 'verb-fonts-v3-' + location.origin;

interface Font {
  path: string;
  extension: string;
  name: string;
  family: string | undefined;
  cacheIdentifier: string;
}

const fonts = [
  {
    path: 'fonts/NotoSans-Regular.TTF',
    extension: 'TTF',
    name: 'Noto Sans Regular',
    family: undefined,
    cacheIdentifier: 'NotoSans-Regular',
  },
  {
    path: 'fonts/NotoSansSC-Regular.OTF',
    extension: 'OTF',
    name: 'Noto Sans Chinese Simplified Regular',
    familiy: 'Noto Sans',
    cacheIdentifier: 'NotoSansSC-Regular',
  },
  {
    path: 'fonts/NotoSansArabic-Regular.TTF',
    extension: 'TTF',
    name: 'Noto Sans Arabic Regular',
    family: undefined,
    cacheIdentifier: 'NotoSansArabic-Regular',
  },
  {
    path: 'fonts/NotoSansGeorgian-Regular.TTF',
    extension: 'TTF',
    name: 'Noto Sans Georgian Regular',
    family: undefined,
    cacheIdentifier: 'NotoSansGeorgian-Regular',
  },
  {
    path: 'fonts/NotoSansJP-Regular.OTF',
    extension: 'OTF',
    name: 'Noto Sans Japanese Regular',
    family: undefined,
    cacheIdentifier: 'NotoSansJP-Regular',
  },
] as Font[];

export const loadingFont = ref(false);
let loadingFontPromise: Promise<void> = Promise.resolve();
export const availableLocaleOptions = ref<LocaleOption[]>([]);

export function updateAvailableLocaleOptions() {
  void getAvailableLocales().then(
    (options) => (availableLocaleOptions.value = options)
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function loadFontForLocale(
  locale: string,
  onError?: (localeLabel: string) => void
): Promise<void> {
  const selectedLocale = localeOptions.find(
    (option) => option.value === locale
  );

  const font = fonts.find((font) => font.name === selectedLocale?.font);

  if (font) {
    loadingFontPromise = loadingFontPromise
      .then(() => {
        loadingFont.value = true;

        return loadFont(font.path).catch(() => {
          if (onError) {
            onError(selectedLocale?.label ?? '');
          }
        });
      })
      .finally(() => {
        loadingFont.value = false;
        updateAvailableLocaleOptions();
      });
  }

  return loadingFontPromise;
}

function cacheEntryBelongsToFont(font: Font) {
  return (cacheEntry: Request) => {
    return (
      new RegExp(font.cacheIdentifier + '\\..*\\.' + font.extension).test(
        cacheEntry.url
      )
    );
  };
}

export function isFontLoaded(locale: string): Promise<boolean> {
  const selectedLocale = localeOptions.find(
    (option) => option.value === locale
  );

  const font = fonts.find((font) => font.name === selectedLocale?.font);

  if (!font) {
    return Promise.resolve(false);
  }

  return window.caches.open(fontCacheName).then((cache) => {
    return cache.keys().then((keys) => {
      return keys.filter(cacheEntryBelongsToFont(font)).length === 1;
    });
  });
}

export function getAvailableLocales(): Promise<LocaleOption[]> {
  return window.caches.open(fontCacheName).then((cache) => {
    return cache.keys().then((keys) => {
      const cachedFonts = fonts
        .filter(
          (font) => keys.filter(cacheEntryBelongsToFont(font)).length === 1
        )
        .map((font) => font.name);

      return localeOptions.filter(
        (locale) =>
          cachedFonts.find((fontName) => fontName === locale.font) !== undefined
      );
    });
  });
}

export function getFontOfLocale(locale: string): Font | undefined {
  const selectedLocale = localeOptions.find(
    (option) => option.value === locale
  );

  return fonts.find((font) => font.name === selectedLocale?.font);
}
