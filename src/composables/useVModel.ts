/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */

import { computed, getCurrentInstance } from 'vue'

export function useVModel<T = any> (props: any, propName: string) {
  const vm = getCurrentInstance()?.proxy

  return computed<T>({
    get () {
      return props[propName]
    },
    set (value) {
      if (vm) {
        vm.$emit(`update:${propName}`, value)
      } else {
        console.log('unable to setup v-model')
      }
    }
  })
}
