import { loadFontForLocale } from 'src/composables/useFonts';
import {
  VersionedSurveySelections,
  SurveyOptionSelection,
} from '../model/Survey';
import { useVerbController } from 'src/composables/useVerbController';
import PDFDocument from 'pdfkit';
import { useQuestionTranslator } from './useQuestionTranslator';
import 'src/register-static-files.js';
import { getFontOfLocale } from './useFonts';
import { localeOptions } from 'src/assets/languages';

interface GermanAndLocaleQuestionWithSelection {
  german: QuestionTextWithSelection;
  locale: QuestionTextWithSelection;
}

interface QuestionTextWithSelection {
  questionText: string;
  selectionText: string;
}

type PDFDoc = typeof PDFDocument;

function loadFontToDoc(
  doc: PDFDoc,
  locale: string,
  onError?: (localeLabel: string) => void
): Promise<void> {
  const font = getFontOfLocale(locale);

  if (font) {
    return loadFontForLocale(locale, onError).then(() => {
      doc.registerFont(font.name, font.path, font.family);
    });
  }

  return Promise.reject();
}

function withLocaleFont(doc: PDFDoc, locale: string): PDFDoc {
  const selectedLocale = localeOptions.find(
    (option) => option.value === locale
  );
  doc.font(selectedLocale?.font ?? 'Noto Sans Regular');

  return doc;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function usePDFExporter(t: any, d: any, locale: any) {
  const controller = useVerbController();
  const translator = useQuestionTranslator(t);

  const pageMargin = 60;
  const germanLocale = 'de';
  const primaryColor = '#c5d8f1';
  const accentColor = '#044674';
  const germanTextColor = '#000000';
  const localeTextColor = '#546e7a';
  let onlyGerman = false;

  function mmToPoints(mm: number): number {
    return mm * 2.8346456693;
  }

  function getBlob(stream: PDFDoc, mimeType: string): Promise<Blob> {
    return new Promise((resolve, reject) => {
      const chunks: BlobPart[] = [];
      stream
        .on('data', (chunk) => chunks.push(chunk as BlobPart))
        .once('end', () => {
          const blob =
            mimeType != null
              ? new Blob(chunks, { type: mimeType })
              : new Blob(chunks);
          resolve(blob);
        })
        .once('error', reject);
    });
  }

  async function toPDF(
    qrcode: string,
    openPDF: (url: string) => void,
    onError?: (localeLabel: string) => void
  ) {
    const doc = new PDFDocument({
      autoFirstPage: false,
      info: {
        Title: 'Web-Entry-Questions-Germany',
        Author: 'Web Entry Questions Germany',
        CreationDate: new Date(),
      },
    });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument
    await loadFontToDoc(doc, locale.value, onError);
    await loadFontToDoc(doc, germanLocale, onError);

    const versionedSurveySelections: VersionedSurveySelections =
      controller.getVersionedSurveySelections();
    const validUntil: Date = controller.getValidUntilDate(
      versionedSurveySelections
    );

    const surveySelections = controller.getSurveySelections();

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument
    buildPages(doc, qrcode, surveySelections, validUntil, locale.value);

    doc.end();

    const blob: Blob = await getBlob(doc, 'application/pdf');
    openPDF(URL.createObjectURL(blob));
  }

  function buildPages(
    doc: PDFDoc,
    qrcode: string,
    surveySelections: SurveyOptionSelection[],
    validUntil: Date,
    locale: string
  ) {
    onlyGerman = locale === germanLocale;

    const page = addNewPage(doc);

    const y = addQRCode(page, mmToPoints(25), qrcode, validUntil, locale);

    addSurveySelections(doc, y + mmToPoints(8), surveySelections, locale);
  }

  function addNewPage(doc: PDFDoc): PDFDoc {
    const page = doc.addPage({
      margins: {
        bottom: 0,
        left: mmToPoints(pageMargin),
        right: mmToPoints(pageMargin),
        top: 0,
      },
      size: 'A4',
    });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument
    addHeader(page, locale.value);
    addFooter(page);

    return page;
  }

  function addHeader(doc: PDFDoc, locale: string) {
    const backgroundX = mmToPoints(0);
    const backgroundY = mmToPoints(0);
    const backgroundWidth = doc.page.width;
    const backgroundHeight = mmToPoints(20);
    const separatorMargin = mmToPoints(2);
    const fontSize = 12;

    doc
      .rect(backgroundX, backgroundY, backgroundWidth, backgroundHeight)
      .fill(primaryColor);

    const logoHeight = mmToPoints(13);
    const logoX = pageMargin;
    const logoOffset = 4;
    const logoY =
      backgroundY + backgroundHeight / 2 - logoHeight / 2 + logoOffset;

    doc.image('images/pdf_header.PNG', logoX, logoY, {
      height: logoHeight,
    });

    doc.fontSize(fontSize);
    doc.fillColor(germanTextColor);

    const selectedLocale = localeOptions.find(
      (option) => option.value === locale
    );
    const germanText = selectedLocale?.germanLabel ?? 'unknown';
    const separatorText = '/';
    const localeText = selectedLocale?.label ?? 'unknown';

    withLocaleFont(doc, germanLocale);
    const germanTextDimensions = getTextDimensions(doc, germanText);

    withLocaleFont(doc, germanLocale);
    const separatorTextDimensions = getTextDimensions(doc, separatorText);

    withLocaleFont(doc, locale);
    const localeTextDimensions = getTextDimensions(doc, localeText);

    const textHeight = Math.max(germanTextDimensions.h, localeTextDimensions.h);
    const textWidth = onlyGerman
      ? germanTextDimensions.w
      : germanTextDimensions.w +
        separatorTextDimensions.w +
        2 * separatorMargin +
        localeTextDimensions.w;

    const localeOffset = 2;
    const localeBorderPaddingX = mmToPoints(5);
    const localeBorderPaddingY = mmToPoints(1.5);
    const localeBorderWidth = textWidth + 2 * localeBorderPaddingX;
    const localeBorderHeight = textHeight + 2 * localeBorderPaddingY;
    const localeBorderX = doc.page.width - localeBorderWidth - pageMargin;
    const localeBorderY =
      backgroundY +
      backgroundHeight / 2 -
      localeBorderHeight / 2 +
      localeOffset;

    const localeX = localeBorderX + (localeBorderWidth - textWidth) / 2;
    const localeY =
      localeBorderY + (localeBorderHeight - textHeight) / 2 + textHeight / 2;

    doc
      .roundedRect(
        localeBorderX,
        localeBorderY,
        localeBorderWidth,
        localeBorderHeight,
        5
      )
      .stroke(accentColor);

    withLocaleFont(doc, germanLocale).text(germanText, localeX, localeY, {
      lineBreak: false,
      baseline: 'middle',
    });

    if (!onlyGerman) {
      withLocaleFont(doc, germanLocale).text(
        separatorText,
        localeX + germanTextDimensions.w + separatorMargin,
        localeY,
        {
          lineBreak: false,
          baseline: 'middle',
        }
      );

      withLocaleFont(doc, locale).text(
        localeText,
        localeX +
          germanTextDimensions.w +
          separatorTextDimensions.w +
          2 * separatorMargin,
        localeY,
        {
          lineBreak: false,
          baseline: 'middle',
        }
      );
    }
  }

  function addFooter(doc: PDFDoc) {
    const height = mmToPoints(45);
    const width = doc.page.width;
    const x = mmToPoints(0);
    const y = doc.page.height - height - mmToPoints(1);

    doc.image('images/pdf_footer.PNG', x, y, {
      height: height,
      width: width,
    });
  }

  function buildQuestionIndicator(
    doc: PDFDoc,
    questionNumber: number,
    questionText: string,
    questionTextLocale: string,
    locale: string,
    x: number
  ) {
    const indicatorSize = mmToPoints(7);
    const indicatorBorderRadius = mmToPoints(1);
    const indicatorMargin = mmToPoints(4);
    const questionNumberFontSize = 12;
    const questionTextFontSize = 10;
    const questionNumberX = x;
    const questionTextOffsetX = indicatorSize + indicatorMargin;
    const questionTextX = x + questionTextOffsetX;
    const questionTextMaxWidth =
      doc.page.width - 2 * pageMargin - questionTextOffsetX;
    const questionLocaleMargin = mmToPoints(0.5);

    doc.fontSize(questionTextFontSize);

    withLocaleFont(doc, germanLocale);
    const questionTextDimensions = getTextDimensions(doc, questionText, {
      width: questionTextMaxWidth,
    });

    withLocaleFont(doc, locale);
    const questionTextLocaleDimensions = getTextDimensions(
      doc,
      questionTextLocale,
      { width: questionTextMaxWidth }
    );

    const print = (y: number) => {
      const questionTextY = y;
      const questionNumberY = y + indicatorSize / 2;

      doc
        .roundedRect(x, y, indicatorSize, indicatorSize, indicatorBorderRadius)
        .fill([197, 216, 241]);

      doc.fillColor(accentColor);
      doc.fontSize(questionNumberFontSize);

      withLocaleFont(doc, germanLocale).text(
        questionNumber.toString(),
        questionNumberX,
        questionNumberY,
        {
          baseline: 'middle',
          align: 'center',
          width: indicatorSize,
        }
      );

      doc.fontSize(questionTextFontSize);
      doc.fillColor(germanTextColor);

      withLocaleFont(doc, germanLocale).text(
        questionText,
        questionTextX,
        questionTextY,
        {
          width: questionTextMaxWidth,
          baseline: 'hanging',
        }
      );

      if (!onlyGerman) {
        doc.fillColor(localeTextColor);

        withLocaleFont(doc, locale).text(
          questionTextLocale,
          questionTextX,
          questionTextY + questionTextDimensions.h + questionLocaleMargin,
          {
            width: questionTextMaxWidth,
            baseline: 'hanging',
          }
        );
      }
    };

    if (onlyGerman) {
      return {
        print,
        questionTextStartX: questionTextX,
        height: questionTextDimensions.h,
      };
    }

    return {
      print,
      questionTextStartX: questionTextX,
      height: questionTextDimensions.h + questionTextLocaleDimensions.h,
    };
  }

  function buildSurveySelection(
    doc: PDFDoc,
    x: number,
    selectionText: string,
    selectionTextLocale: string,
    locale: string
  ) {
    const selectionTextFontSize = 9;
    const slimTextSeperator = '|';
    const slimTextSeparatorMargin = mmToPoints(3);
    const notSlimTextMargin = mmToPoints(3);
    const selectionTextMaxWidth = doc.page.width - 2 * pageMargin - x;
    let computedWidth = 0;
    let computedHeight = 0;
    let selectionTextLocaleOffsetY = 0;
    let selectionTextLocaleOffsetX = 0;

    doc.fontSize(selectionTextFontSize);

    withLocaleFont(doc, germanLocale);
    const selectionTextDimensions = getTextDimensions(doc, selectionText, {
      width: selectionTextMaxWidth,
      baseline: 'hanging',
    });

    withLocaleFont(doc, germanLocale);
    const slimTextSeparatorDimensions = getTextDimensions(
      doc,
      slimTextSeperator,
      {
        width: selectionTextMaxWidth,
        baseline: 'hanging',
      }
    );

    withLocaleFont(doc, locale);
    const selectionTextLocaleDimensions = getTextDimensions(
      doc,
      selectionTextLocale,
      {
        width: selectionTextMaxWidth,
        baseline: 'hanging',
      }
    );

    const slimTextDimensions = onlyGerman
      ? {
          w: selectionTextDimensions.w,
          h: selectionTextDimensions.h,
        }
      : {
          w:
            selectionTextDimensions.w +
            slimTextSeparatorDimensions.w +
            2 * slimTextSeparatorMargin +
            selectionTextLocaleDimensions.w,
          h: Math.max(
            selectionTextDimensions.h,
            selectionTextLocaleDimensions.h
          ), // can only be a one-liner
        };

    let useSlimText = false;
    if (slimTextDimensions.w > selectionTextMaxWidth) {
      selectionTextLocaleOffsetY = selectionTextDimensions.h;

      if (onlyGerman) {
        computedWidth = selectionTextDimensions.w;
        computedHeight = selectionTextDimensions.h;
      } else {
        computedWidth = Math.max(
          selectionTextDimensions.w,
          selectionTextLocaleDimensions.w
        );
        computedHeight =
          selectionTextDimensions.h +
          selectionTextLocaleDimensions.h +
          notSlimTextMargin;
      }
    } else {
      selectionTextLocaleOffsetX = selectionTextDimensions.w;

      useSlimText = true;

      computedWidth = slimTextDimensions.w;
      computedHeight = slimTextDimensions.h;
    }

    const print = (y: number) => {
      doc.fillColor(germanTextColor);
      doc.fontSize(selectionTextFontSize);

      withLocaleFont(doc, germanLocale).text(selectionText, x, y, {
        width: selectionTextMaxWidth,
        baseline: 'hanging',
      });

      if (!onlyGerman) {
        doc.fillColor(localeTextColor);

        y += selectionTextLocaleOffsetY;
        x += selectionTextLocaleOffsetX;

        if (useSlimText) {
          x += slimTextSeparatorMargin;

          withLocaleFont(doc, germanLocale).text(slimTextSeperator, x, y, {
            baseline: 'hanging',
          });

          x += slimTextSeparatorDimensions.w + slimTextSeparatorMargin;
        } else {
          y += notSlimTextMargin;
        }

        withLocaleFont(doc, locale).text(selectionTextLocale, x, y, {
          width: selectionTextMaxWidth,
          baseline: 'hanging',
        });
      }
    };

    return {
      print,
      width: computedWidth,
      height: computedHeight,
    };
  }

  function addSurveySelections(
    doc: PDFDoc,
    y: number,
    surveySelections: SurveyOptionSelection[],
    locale: string
  ) {
    let currentYPosition = y;
    const translatedSelectionTexts = getTranslatedSelectionTexts(
      surveySelections,
      locale
    );

    translatedSelectionTexts.forEach((question, index) => {
      const selectionMargin = mmToPoints(3);
      const questionMargin = mmToPoints(7.5);
      const pageBreakHeight = mmToPoints(258);

      const questionIndicator = buildQuestionIndicator(
        doc,
        index + 1,
        question.german.questionText,
        question.locale.questionText,
        locale,
        pageMargin
      );

      const surveySelection = buildSurveySelection(
        doc,
        questionIndicator.questionTextStartX,
        question.german.selectionText,
        question.locale.selectionText,
        locale
      );

      const endOfSelection =
        currentYPosition + questionIndicator.height + surveySelection.height;

      if (endOfSelection >= pageBreakHeight) {
        doc = addNewPage(doc);
        currentYPosition = mmToPoints(32);
      }

      questionIndicator.print(currentYPosition);
      surveySelection.print(
        currentYPosition + questionIndicator.height + selectionMargin
      );

      currentYPosition +=
        questionIndicator.height + surveySelection.height + questionMargin;
    });
  }

  function getTranslatedSelectionTexts(
    surveySelections: SurveyOptionSelection[],
    locale: string
  ): GermanAndLocaleQuestionWithSelection[] {
    return surveySelections.map((selection, index) => {
      let selectionArray: number[];

      if (typeof selection === 'number') {
        selectionArray = [selection];
      } else if (selection instanceof Array) {
        selectionArray = selection;
      } else {
        throw new Error('undefined selection');
      }

      return {
        german: {
          questionText: translator.surveyQuestionText(index, germanLocale),
          selectionText: selectionArray
            .map((selection) =>
              translator.surveyOptionText(index, selection, germanLocale)
            )
            .join(', '),
        },
        locale: {
          questionText: translator.surveyQuestionText(index, locale),
          selectionText: selectionArray
            .map((selection) =>
              translator.surveyOptionText(index, selection, locale)
            )
            .join(', '),
        },
      };
    });
  }

  function getTextDimensions(
    doc: PDFDoc,
    text: string,
    options?: PDFKit.Mixins.TextOptions
  ): { w: number; h: number } {
    const maxPageWidth = doc.page.width - 2 * pageMargin;

    return {
      w: Math.min(doc.widthOfString(text, {
        width: maxPageWidth,
        ...options
      }), maxPageWidth),
      h: doc.heightOfString(text, {
        width: maxPageWidth,
        ...options
      }),
    };
  }

  function addQRCode(
    doc: PDFDoc,
    y: number,
    qrCode: string,
    validUntil: Date,
    locale: string
  ) {
    const qrCodeBorderStrokeWidth = mmToPoints(3);
    const qrCodeSize = mmToPoints(60);
    const qrCodeBorderRadius = mmToPoints(3);
    const qrCodeBorderSize = mmToPoints(80);
    const qrCodeBorderX = (doc.page.width - qrCodeBorderSize) / 2;
    const qrCodeBorderY = y;
    const qrCodeX = (doc.page.width - qrCodeSize) / 2;
    const qrCodeY = qrCodeBorderY + mmToPoints(10);

    doc.image(qrCode, qrCodeX, qrCodeY, { cover: [qrCodeSize, qrCodeSize] });

    const validUntilFontSize = 11;

    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
    const validUntilText: string = t(
      'survey_selections_valid_until',
      {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
        date: d(validUntil, 'long', locale),
      },
      { locale: germanLocale }
    );

    doc.fontSize(validUntilFontSize);

    withLocaleFont(doc, germanLocale);
    const validUntilTextDimensions = getTextDimensions(doc, validUntilText);

    const pilotInfoFontSize = 12;

    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
    const pilotInfoText: string = t('ht_pilot_short', ['\n']);

    const pilotInfoY =
      qrCodeBorderY +
      qrCodeBorderSize +
      qrCodeBorderStrokeWidth +
      validUntilTextDimensions.h +
      mmToPoints(2);

    doc.fontSize(pilotInfoFontSize);

    withLocaleFont(doc, locale);
    const pilotInfoTextDimensions = getTextDimensions(doc, pilotInfoText);

    doc.fillColor(primaryColor);
    doc.strokeColor(primaryColor).lineWidth(qrCodeBorderStrokeWidth);
    doc
      .roundedRect(
        qrCodeBorderX - qrCodeBorderStrokeWidth / 2,
        qrCodeBorderY + qrCodeBorderSize,
        qrCodeBorderSize + qrCodeBorderStrokeWidth,
        validUntilTextDimensions.h + 4,
        qrCodeBorderRadius
      )
      .fill();

    doc
      .rect(
        qrCodeBorderX - qrCodeBorderStrokeWidth / 2,
        qrCodeBorderY + qrCodeBorderSize - qrCodeBorderStrokeWidth,
        qrCodeBorderStrokeWidth,
        qrCodeBorderStrokeWidth * 2
      )
      .fill();

    doc
      .rect(
        qrCodeBorderX + qrCodeBorderSize - qrCodeBorderStrokeWidth / 2,
        qrCodeBorderY + qrCodeBorderSize - qrCodeBorderStrokeWidth,
        qrCodeBorderStrokeWidth,
        qrCodeBorderStrokeWidth * 2
      )
      .fill();

    doc
      .roundedRect(
        qrCodeBorderX,
        qrCodeBorderY,
        qrCodeBorderSize,
        qrCodeBorderSize,
        qrCodeBorderRadius
      )
      .stroke();

    doc.fillColor(germanTextColor);
    doc.fontSize(validUntilFontSize);
    withLocaleFont(doc, germanLocale).text(
      validUntilText,
      (doc.page.width - validUntilTextDimensions.w) / 2,
      qrCodeBorderY + qrCodeBorderSize,
      {
        baseline: 'top',
        align: 'left',
      }
    );

    doc.fillColor(accentColor);
    doc.fontSize(pilotInfoFontSize);

    withLocaleFont(doc, locale).text(
      pilotInfoText,
      (doc.page.width - pilotInfoTextDimensions.w) / 2,
      pilotInfoY,
      {
        width: doc.page.width - 2 * pageMargin
      }
    );

    return pilotInfoY + pilotInfoTextDimensions.h;
  }

  return {
    toPDF,
  };
}
