import arAR from './ar-AR';
import deDE from './de-DE';
import enUS from './en-US';
import esES from './es-ES';
import frFR from './fr-FR';
import hiHI from './hi-HI';
import jaJA from './ja-JA';
import kaKA from './ka-KA';
import ptPT from './pt-PT';
import ruRU from './ru-RU';
import sqSQ from './sq-SQ';
import trTR from './tr-TR';
import zhZH from './zh-ZH';

export default {
  ar: arAR,
  de: deDE,
  en: enUS,
  es: esES,
  fr: frFR,
  hi: hiHI,
  ja: jaJA,
  ka: kaKA,
  pt: ptPT,
  ru: ruRU,
  sq: sqSQ,
  tr: trTR,
  zh: zhZH,
};
