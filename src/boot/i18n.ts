import { boot } from 'quasar/wrappers';
import { createI18n } from 'vue-i18n';
import { localeOptions } from 'src/assets/languages';
import messages from 'src/i18n';
import { Quasar } from 'quasar';

export default boot(({ app }) => {
  const selectedLocale = localStorage.getItem('selectedLocale');
  let locale = selectedLocale ? selectedLocale : Quasar.lang.getLocale();

  locale = locale?.split('-')[0];

  const languagePack = localeOptions.find((e) => e.value === locale);

  if (languagePack) {
    Quasar.lang.set(languagePack.lang);
  }

  const i18n = createI18n({
    legacy: false,
    locale: locale,
    fallbackLocale: 'en',
    messages,
    datetimeFormats: {
      en: {
        short: {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
        },
        long: {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit',
        },
      },
      de: {
        short: {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
        },
        long: {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit',
        },
      },
    },
  });

  // Set i18n instance on app
  app.use(i18n);
});
