import { QuasarLanguage } from 'quasar';
import ar from 'quasar/lang/ar';
import de from 'quasar/lang/de';
import en from 'src/assets/en';
import es from 'quasar/lang/es';
import fr from 'quasar/lang/fr';
import hi from 'src/assets/hi';
import ja from 'quasar/lang/ja';
import ka from 'src/assets/ka';
import pt from 'quasar/lang/pt';
import ru from 'quasar/lang/ru';
import sq from 'src/assets/sq';
import tr from 'quasar/lang/tr';
import zh from 'src/assets/zh';

const germanLabels = {
  ar: 'Arabisch',
  de: 'Deutsch',
  en: 'Englisch',
  es: 'Spanisch',
  fr: 'Französisch',
  hi: 'Hindi',
  ja: 'Japanisch',
  ka: 'Georgisch',
  pt: 'Portugiesisch',
  ru: 'Russisch',
  sq: 'Albanisch',
  tr: 'Türkisch',
  zh: 'Chinesisch',
} as { [index: string]: string };

const localeFonts = {
  ar: 'Noto Sans Arabic Regular',
  de: 'Noto Sans Regular',
  en: 'Noto Sans Regular',
  es: 'Noto Sans Regular',
  fr: 'Noto Sans Regular',
  hi: 'Noto Sans Regular',
  ja: 'Noto Sans Japanese Regular',
  ka: 'Noto Sans Georgian Regular',
  pt: 'Noto Sans Regular',
  ru: 'Noto Sans Regular',
  sq: 'Noto Sans Regular',
  tr: 'Noto Sans Regular',
  zh: 'Noto Sans Chinese Simplified Regular',
} as { [index: string]: string };

export interface LocaleOption {
  value: string;
  label: string;
  germanLabel: string;
  lang: QuasarLanguage;
  font: string;
}

export const localeOptions = [
  ar,
  de,
  en,
  es,
  fr,
  hi,
  ja,
  ka,
  pt,
  ru,
  sq,
  tr,
  zh,
].map((lang) => {
  return {
    value: lang.isoName,
    label: lang.nativeName,
    germanLabel: germanLabels[lang.isoName],
    lang: lang,
    font: localeFonts[lang.isoName],
  };
}) as LocaleOption[];
