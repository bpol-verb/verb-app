export interface PrecheckOption {
  action: string | null;
}

export interface NullablePrecheckOption {
  option: PrecheckOption | null;
}

export interface PrecheckQuestion {
  options: PrecheckOption[];
}

export interface SurveyQuestion {
  type: string;
  options: number;
}

export interface Questionaire {
  version: number;
  precheck: PrecheckQuestion[];
  survey: SurveyQuestion[];
}

export type VersionedSurveySelections = (number | number[])[];

export type SurveyOptionSelection = number | number[];
